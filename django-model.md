# Django model

-   Django models are like blueprints for organizing and managing data in a web application.

-   Each model represents a specific type of data, and it consists of fields that define the attributes or characteristics of that data. For example, a user model might have fields like username, email, and password.

-   These models help Django create and manage a database behind the scenes.
-   When you define a model, Django automatically generates SQL code to create the corresponding database tables.
-   Also it provides you with tools to interact with those tables, such as querying, inserting, updating, and deleting data.

## What is ondelete Cascade?

-   In Django models, the `on_delete` parameter is used to specify what happens to related objects when the referenced object is deleted.
-   When you have a ForeignKey or a OneToOneField in your model, you often need to decide what to do with related objects if the referenced object is deleted.
-   `on_delete = CASCADE` is one of the options available for the `on_delete` parameter.
-   When you set it to `CASCADE` it means that is the referenced object is deleted, all related objects will also be deleted automatically.
-   This ensures data consistency and prevents orphaned records in your database.

## Fields and validators available in django-models

The most important part of a model and the only required part of a model is the list of database fields it defines. Fields are specified by class attributes.

### Basic model data types and fields list

1. **AutoField**

    - It is an IntegerField that automatically increments.

2. **BigIntegerField**

    - It is a 64-bit integer, much like an IntegerField except that it is guaranteed to fit numbers from -9223372036854775808 to 9223372036854775807.

3. **BinaryField**

    - A field to store raw binary data.

4. **BooleanField**

    - A true/false field. The default form widget for this field is a CheckboxInput.

5. **CharField**

    - A field to store text-based values.

6. **DateField**

    - A date, represented in Python by a datetime.date instance.

7. **DateTimeField**

    - It is used for date and time, represented in Python by a datetime.datetime instance.

8. **DecimalField**

    - It is a fixed-precision decimal number, represented in Python by a Decimal instance.

9. **EmailField**

    - It is a CharField that checks that the value is a valid email address.

10. **FileField**

    - It is a file-upload field.

11. **FloatField**

    - It is a floating-point number represented in Python by a float instance.

12. **ImageField**

    - It inherits all attributes and methods from FileField, but also validates that the uploaded object is a valid image.

13. **IntegerField**

    - It is an integer field. Values from -2147483648 to 2147483647 are safe in all databases supported by Django.

14. **GenericIPAddressField**

    - An IPv4 or IPv6 address, in string format (e.g. 192.0.2.30 or 2a02:42fe::4).

15. **NullBooleanField**

    - Like a BooleanField, but allows NULL as one of the options.

16. **TextField**
    - A large text field. The default form widget for this field is a Textarea.

## Difference between python module and python class

1. **Module**:

    - A module is a file containing Python code that can define functions, classes, and variables. It serves as a container for organizing and reusing related code.

2. **Class**:
    - A class is a blueprint for creating objects. It defines the attributes (properties) and methods (functions) that objects of that class will have. Classes enable the creation of objects with specific behaviors and characteristics.
