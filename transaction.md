# Transactions

**SQL Transactions:**
1. A sequence of one or more SQL operations treated as a single unit of work.
2. Ensures data integrity and consistency in database management systems.
3. Allows multiple operations to be executed as a single, indivisible unit.
4. Either all operations within the transaction are completed successfully and permanently saved to the database, or none of them are.

**Atomic Transactions:**
1. One of the ACID properties of database transactions.
2. Ensures that all operations within a transaction are performed as a single, indivisible unit.
3. Either all operations succeed and the transaction is committed, or if any operation fails, the entire transaction is rolled back.
4. Guarantees that the database is left unchanged if a transaction fails, maintaining data integrity.

**Example of Atomic Transaction in Django:**
```python
from django.db import transaction

# Define a function to transfer funds between accounts atomically
# using deocorator
@transaction.atomic
def transfer_funds(sender_account, recipient_account, amount):
    sender_account.balance -= amount
    recipient_account.balance += amount
    sender_account.save()
    recipient_account.save()

# using django context manager

def transfer_funds(sender_account, recipient_account, amount):
    with transaction.atomic():
        sender_account.balance -= amount
        recipient_account.balance += amount
        sender_account.save()
        recipient_account.save()


# Usage example
sender = Account.objects.get(id=1)
recipient = Account.objects.get(id=2)
amount = 100
transfer_funds(sender, recipient, amount)
```

In this example:
- The `transfer_funds` function is decorated with `@transaction.atomic`, ensuring that all operations within the function are treated as an atomic transaction.
- If any error occurs during the execution (e.g., a database constraint violation), Django will automatically roll back the entire transaction, ensuring that the sender's account balance is not deducted and the recipient's account balance is not increased, maintaining data integrity.