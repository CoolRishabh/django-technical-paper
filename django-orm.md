# Django ORM

Django ORM (Object-Relational Mapping) is a powerful feature of the Django web framework that allows developers to interact with a relational database using high-level Python objects instead of directly writing SQL queries. 

1. **Model Definition**:
   - In Django, you define your database schema using Python classes called models. Each model class represents a table in the database, and each attribute of the class represents a column in that table.

   ```python
   from django.db import models

   class User(models.Model):
       username = models.CharField(max_length=100)
       email = models.EmailField()
       age = models.IntegerField()
   ```

2. **Migration**:
   - After defining your models, you need to create corresponding database tables. Django provides a tool called `makemigrations` to generate migration files based on the changes you've made to your models, and then you apply these migrations to your database using the `migrate` command.

   ```bash
   python manage.py makemigrations
   python manage.py migrate
   ```

3. **Querying**:
   - Once your models are defined and the database is set up, you can interact with the database using the Django ORM. You can perform CRUD (Create, Read, Update, Delete) operations on your database tables using Python code.

   ```python
   # Creating a new user
   user = User.objects.create(username='john', email='john@example.com', age=30)

   # Querying for users
   users = User.objects.filter(age__gte=25)

   # Updating a user
   user.age = 40
   user.save()

   # Deleting a user
   user.delete()
   ```

4. **Relationships**:
   - Django ORM supports defining relationships between models, such as one-to-one, one-to-many, and many-to-many relationships. These relationships are represented using `ForeignKey`, `OneToOneField`, and `ManyToManyField` respectively.

   ```python
   class Post(models.Model):
       title = models.CharField(max_length=100)
       content = models.TextField()
       author = models.ForeignKey(User, on_delete=models.CASCADE)
   ```

Django ORM abstracts away much of the complexity of working with databases, making it easier and faster to develop database-driven web applications in Python.

## Aggregation and Annotations in django ORM queries?

Aggregations and annotations are both powerful features in Django's ORM (Object-Relational Mapping) that allow you to perform complex operations on querysets.

1. **Aggregations**: Aggregations in Django allow you to perform operations across multiple rows of a queryset and return a single result. Common aggregation functions include `Sum`, `Count`, `Avg`, `Min`, and `Max`. These functions are applied to groups of objects and return a summary value. For example, you can use the `Sum` aggregation to calculate the total value of a particular field across multiple objects in a queryset.

   ```python
   from django.db.models import Sum
   from myapp.models import Expense

   total_expenses = Expense.objects.aggregate(total=Sum('amount'))
   ```

2. **Annotations**: Annotations, on the other hand, allow you to add extra information to each object in a queryset, based on calculations or other data from related models. Annotations are similar to aggregations, but instead of returning a single result, they add a new attribute to each object in the resulting queryset.

   ```python
   from django.db.models import F
   from myapp.models import Product

   # Annotate each product with its discounted price
   products_with_discount = Product.objects.annotate(discounted_price=F('price') * 0.9)
   ```

In summary, aggregations are used to perform calculations across multiple rows and return a single result, while annotations are used to add extra information to each object in a queryset based on calculations or data from related models. Both are powerful tools for performing complex operations in Django's ORM.